let inputCode = document.getElementById("code");
let inputInternalCode = document.getElementById("internalCode");
let inputDescription = document.getElementById("description");
let inputBrand = document.getElementById("brand");

let searchButtonCode = document.getElementById("searchButtonCode");
let searchButtonInternalCode = document.getElementById("searchButtonInternalCode");
let searchButtonDescription = document.getElementById("searchButtonDescription");
let searchButtonBrand = document.getElementById("searchButtonBrand");

let filterCode = document.getElementById("filterCode");
let filterInternalCode = document.getElementById("filterInternalCode");
let filterDescription = document.getElementById("filterDescription");

let headerInputCode = document.getElementById("headerInputCode");
let headerInputInternalCode = document.getElementById("headerInputInternalCode");
let headerInputDescription = document.getElementById("headerInputDescription");
let headerInputBrand = document.getElementById("headerInputBrand");

let inactiveInput = document.getElementById("inactive");
let columnName = document.getElementById("column-name");
let results = document.getElementById("results");

let inactive = 1;
let URL = "produtos";
let index = 0;

function setFilter(event) {
  if (event.target === filterCode) getFilter(filterCode);
  if (event.target === filterInternalCode) getFilter(filterInternalCode);
  if (event.target === filterDescription) getFilter(filterDescription);

  if (inactiveInput.checked) {
    inactive = 0;
  } else {
    inactive = 1;
  }
}

function getFilter(filter) {
  filterClassList(filter);
  clearInput();
}

function filterClassList(filter) {
  filter.classList.add("active");
  if (filter === filterCode) {
    columnName.classList.add("column-name-code");
    columnName.classList.remove("column-name-internal-code");

    headerInputCode.classList.add("active");
    headerInputInternalCode.classList.remove("active");
    headerInputDescription.classList.remove("active");
    headerInputBrand.classList.remove("active");

    filterDescription.classList.remove("active");
    filterInternalCode.classList.remove("active");
  } else if (filter === filterInternalCode) {
    columnName.classList.add("column-name-internal-code");
    columnName.classList.remove("column-name-code");

    headerInputInternalCode.classList.add("active");
    headerInputCode.classList.remove("active");
    headerInputDescription.classList.remove("active");
    headerInputBrand.classList.remove("active");

    filterCode.classList.remove("active");
    filterDescription.classList.remove("active");
  } else if (filter === filterDescription) {
    columnName.classList.add("column-name-code");
    columnName.classList.remove("column-name-internal-code");

    headerInputDescription.classList.add("active");
    headerInputBrand.classList.add("active");
    headerInputCode.classList.remove("active");
    headerInputInternalCode.classList.remove("active");

    filterCode.classList.remove("active");
    filterInternalCode.classList.remove("active");
  }
}

inputCode.addEventListener("keyup", (event) => {
  if (inputCode.value != "") {
    filter("code");
  } else {
    filter("notFilter");
  }
});

inputInternalCode.addEventListener("keyup", (event) => {
  if (inputInternalCode.value != "") {
    filter("internalCode");
  } else {
    filter("notFilter");
  }
});

inputDescription.addEventListener("keyup", (event) => {
  if (inputDescription.value != "") {
    filter("description");
  } else {
    filter("notFilter");
  }
});

inputBrand.addEventListener("keyup", (event) => {
  if (inputBrand.value != "" && inputDescription.value != "") {
    filter("brand");
  } else {
    filter("notFilter");
  }
});

searchButtonCode.addEventListener("click", () => {
  filter("code");
});

searchButtonInternalCode.addEventListener("click", () => {
  filter("internalCode");
});

searchButtonDescription.addEventListener("click", () => {
  filter("description");
});

searchButtonBrand.addEventListener("click", () => {
  filter("brand");
});

function filter(filter) {
  switch (filter) {
    case "code":
      URL = `produtos/cod/${inputCode.value}/ativo/${inactive}`;
      break;
    case "internalCode":
      URL = `produtos/cod_interno/${inputInternalCode.value}/ativo/${inactive}`;
      break;
    case "description":
      URL = `produtos/descricao/${inputDescription.value}/ativo/${inactive}`;
      break;
    case "brand":
      URL = `produtos/descricao/${inputDescription.value}/marca/${inputBrand.value}/ativo/${inactive}`;
      break;
    case "notFilter":
      URL = `produtos`;
      break;
  }
  updateTable();
}

function createRow(client) {
  let activity = "Inativo";
  if (client.ativo === 1) {
    activity = "Ativo";
  }
  const newRow = document.createElement("div");
  newRow.classList.add("results__content");
  newRow.innerHTML = `
  <div id="row-${client.codigo}" class="results__row active inactive-${client.ativo}" onclick="functionA(${client.codigo})">
    <span>${client.codigo}</span>
    <span>${client.descricao}</span>
    <span>${client.marcas_nomeMarca}</span>
    <span>${client.qtd_estoque}</span>
    <span>${client.preco_compra}</span>
    <span>${client.preco_venda}</span>
  </div>
  <div class="detail" id="detail-${client.codigo}">${client.codigo}</div>
  <div id="row-details-${client.codigo}" class="results__row--details" onclick="functionA(${client.codigo})">
    <div>
      <span>descrição</span>
      <div>${client.descricao}</div>
    </div>
    <div>
      <span>fornecedor</span>
      <div>${client.fornecedores_nomeFornecedor}</div>
    </div>
    <div>
      <span>preço de custo</span>
      <div>${client.preco_compra}</div>
    </div>
    <div>
      <span>marca</span>
      <div>${client.marcas_nomeMarca}</div>
    </div>
    <div>
      <span>código interno</span>
      <div>${client.codigo_interno}</div>
    </div>
    <div>
      <span>preço de venda</span>
      <div>${client.preco_venda}</div>
    </div>
    <div>
      <span>lucro</span>
      <div>${client.margem_lucro * 100}%</div>
    </div>
    <div>
      <span>estoque</span>
      <div>${client.qtd_estoque}</div>
    </div>
    <div> 
      <span>${activity}</span>
    </div>
    <div>
    <a href="#" onClick="modal(${client.codigo})" >Ver Mais</a>
    </div>
  </div>
`;

  results.appendChild(newRow);

}

function modal(client){
  window.open(
    `./../detalhes/produto/produto.html?codigo=${client}`,
    'Detalhes Produto',
    'resizable,height=622,width=1100,top=200, left=500');
    return false;
}

function clearInput() {
  inputCode.value = "";
  inputInternalCode.value = "";
  inputDescription.value = "";
  inputBrand.value = "";
}

function clearTable() {
  const rows = document.querySelectorAll("#results div");
  rows.forEach((row) => row.parentNode.removeChild(row));
}

function updateTable() {
  clearTable();
  fetch(`http://localhost:3000/${URL}`)
    .then((res) => res.json())
    .then((data) => {
      data.forEach(createRow);
    });
}

let clickOneTime = 0;
let clickTwoTime = new Date();
let timeDiff = 0;


function functionA(id) {
  clickOneTime = new Date()
  timeDiff =  (clickOneTime - clickTwoTime) / 1000
  if (timeDiff < 0.5) {
    modal(id)
  }

  clickTwoTime = clickOneTime

  let rowDetails = document.getElementById("row-details-" + id);
  let detail = document.getElementById("detail-" + id);
  let row = document.getElementById("row-" + id);

  let indexCurrent;
  indexCurrent = id;

  if (indexCurrent != index) {
    rowDetails.classList.add("active");
    detail.classList.add("active");
    row.classList.remove("active");
    if (index != 0) {
      let detailOld = document.getElementById("detail-" + index);
      let rowDetailsOld = document.getElementById("row-details-" + index);
      let rowOld = document.getElementById("row-" + index);
      rowDetailsOld.classList.remove("active");
      rowOld.classList.add("active");
      rowDetailsOld = rowDetails;

      if (detailOld != detail) {
        detailOld.classList.remove("active");
      }
    }
  }

  if (indexCurrent === index) {
    detail.classList.toggle("active");
    rowDetails.classList.toggle("active");
    row.classList.toggle("active");
  }

  index = indexCurrent;
}

updateTable();

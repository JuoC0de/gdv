let navbarVendas = document.getElementById("navbar-vendas");
let navbarCadastros = document.getElementById("navbar-cadastros");
let navbarSecondaryVendas = document.getElementById("nav-secondary-vendas");
let navbarSecondaryCadastros = document.getElementById(
  "nav-secondary-cadastros"
);

function setNavbarMouseover(event) {
  if (event.target === navbarVendas) {
    navbarMouseover(navbarSecondaryVendas, navbarVendas);
  }
  if (event.target === navbarCadastros) {
    navbarMouseover(navbarSecondaryCadastros, navbarCadastros);
  }
}

function navbarMouseover(navbarSecondary, navbarItem) {
  navbarSecondary.classList.add("active");
  navbarSecondary.addEventListener("mouseover", () => {
    navbarSecondary.classList.add("active");
  });
  navbarSecondary.addEventListener("mouseout", () => {
    navbarSecondary.classList.remove("active");
  });
  navbarItem.addEventListener("mouseout", () => {
    navbarSecondary.classList.remove("active");
  });
}

const form = document.getElementById("form");
const ativo = document.getElementById("ativo");
const label_input = document.getElementById("activity");

form.addEventListener("submit", (event) => {
  event.preventDefault();
  const formData = new FormData(form);
  let data = Object.fromEntries(formData);
  ativo.checked ? data.ativo = 1 : data.ativo = 0;
  fetch('http://localhost:3000/produto', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  .then(res => res.json())
  .then(res => {
    console.log(res)
    form.reset()
  })
  .catch(error => console.log(error))
});

function isActive(){
  if(ativo.checked) {
    label_input.classList.add("label_ativo")
    label_input.innerText = "Ativo"
  } else {
    label_input.classList.remove("label_ativo")
    label_input.innerText = "Inativo"
  }
}
const modal_data = document.getElementById("modal_data")

let URL = ''

function getParams() {
    let params = window.location.search.substring(1).split('=')
    URL = `produto/cod/${params[1]}/`

}

function updateTable() {
    getParams()
    fetch(`http://localhost:3000/${URL}`)
      .then((res) => res.json())
      .then((data) => {
        create_data(data[0])
    });
}


function create_data(client){
    const section = document.createElement("section");
    section.innerHTML=`
    <div>
        <label>Descrição</label>
        <span>${client.descricao}</span> 
    </div>
    <div>
        <label>CSOSN</label>
        <span>${client.csosn}</span>
    </div>
    <div>
        <label>Código</label>
        <span>${client.codigo}</span>
    </div>
    <div>
        <label>Preço Compra</label>
        <span>${client.preco_compra}</span>
    </div>
    <div>
        <label>NMC</label>
        <span>${client.nmc}</span>
    </div>
    <div>
        <label>Marca</label>
        <span>${client.marcas_nomeMarca}</span>
    </div>
    <div>
        <label>Preço Venda</label>
        <span>${client.preco_venda}</span>
    </div>
    <div>
        <label>CEST</label>
        <span>${client.cest}</span>
    </div>
    <div>
        <label>Fornecedor</label>
        <span>${client.fornecedores_nomeFornecedor}</span>
    </div>
    <div>
        <label>Margem lucro</label>
        <span>${client.margem_lucro}</span>
    </div>
    <div>
        <label>Alíquota de ICMS</label>
        <span>${client.aliquota_icms}</span>
    </div>
    <div>
        <label>Código Interno</label>
        <span>${client.codigo_interno}</span>
    </div>
    <div>
        <label>Quantidade Estoque</label>
        <span>${client.qtd_estoque}</span>
    </div>
    <div>
        <label>Calculo ICMS</label>
        <span>${client.base_icms}</span>
    </div>
    <div>
        <label>Reajuste de Preço</label>
        <span>${client.data_ultimo_reajuste_preco}</span>
    </div>
    <div>
        <label>Minimo em Estoque</label>
        <span>${client.min_estoque}</span>
    </div>
    <div>
        <label>Alíquota do IPI</label>
        <span>${client.ipi}</span>
    </div>
    <div>
        <label>Observação</label>
        <span>${client.observacoes}</span>
    </div>
    <div>
        <label>Código GTIN</label>
        <span>${client.gtin}</span>
    </div>
    <div>
    <div class="forminput__checkbox">
        <label><input type="checkbox" disabled  id="Ativo"></label>
        <label id="activity"></label>
    </div>
    </div>
    `

    modal_data.appendChild(section)

    activityCheck(client)
}

function activityCheck(client){
    let ativo = document.getElementById('Ativo')
    let activity = document.getElementById('activity')

    let atividade = client.ativo

    console.log(atividade)

    if(atividade == 1) {
        var activi = document.createTextNode('Ativo')
        activity.classList.add("active")           
        ativo.setAttribute('checked','true') 
    } else {
        var activi = document.createTextNode('Desativado')
    } 
    activity.appendChild(activi)
}


updateTable()


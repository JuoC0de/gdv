const { stringify_object_values, stringify_object_keys } = require('../utilities/string_funcitions')
const sql = require('mysql2')
const banco = sql.createPool({
    database: 'gdv',
    user: 'root',
    password: '',
    host: 'localhost',
    port: '3306'
})

class ProdutosSQL {
    static buscar_produtos = () => 
    new Promise((resolve, reject) => {
        const QUERY = 'select * from produtos'
        
        banco.getConnection((error, conn) => {
            if (error) reject(error)
            
            conn.query(QUERY, (erro, results) => {
                conn.release()
                if(erro) reject(erro)
                
                resolve(results)
            })
        })
    })

    static buscar_produto_por_codigo = (codigo) => 
    new Promise((resolve, reject) => {
        const QUERY = `select * from produtos where codigo = '${codigo}'`
        banco.getConnection((error, conn) => {
            if (error) reject(error)
            
            conn.query(QUERY, (erro, results) => {
                conn.release()
                if(erro) reject(erro)
                
                resolve(results)
            })
        })
    })

    static buscar_produtos_por_codigo = (codigo, ativo) => 
    new Promise((resolve, reject) => {
        const QUERY = `select * from produtos where codigo like '${codigo}%' and ativo = ${ativo}`
        banco.getConnection((error, conn) => {
            if (error) reject(error)
            
            conn.query(QUERY, (erro, results) => {
                conn.release()
                if(erro) reject(erro)
                
                resolve(results)
            })
        })
    })

    static buscar_produtos_por_codigo_interno = (codigo_interno, ativo) => 
    new Promise((resolve, reject) => {
        const QUERY = `select * from produtos where codigo_interno like '${codigo_interno}%' and ativo = ${ativo}`
        banco.getConnection((error, conn) => {
            if (error) reject(error)
            
            conn.query(QUERY, (erro, results) => {
                conn.release()
                if(erro) reject(erro)
                
                resolve(results)
            })
        })
    })

    static buscar_produtos_por_descricao = (descricao, ativo) => 
    new Promise((resolve, reject) => {
        const QUERY = `select * from produtos where descricao like '%${descricao}%' and ativo = ${ativo}`
        banco.getConnection((error, conn) => {
            if (error) reject(error)
            
            conn.query(QUERY, (erro, results) => {
                conn.release()
                if(erro) reject(erro)
                
                resolve(results)
            })
        })
    })

    static buscar_produtos_por_descricao_e_marca = (descricao, marca, ativo) => 
    new Promise((resolve, reject) => {
        const QUERY = `select * from produtos where descricao like '%${descricao}%' and marcas_nomeMarca like '%${marca}%' and ativo = ${ativo}`
        banco.getConnection((error, conn) => {
            if (error) reject(error)
            
            conn.query(QUERY, (erro, results) => {
                conn.release()
                if(erro) reject(erro)
                
                resolve(results)
            })
        })
    })

    static cadastrar_produto = (novo_produto) =>
    new Promise((resolve, reject) => {
        const QUERY = `insert into produtos(${stringify_object_keys(novo_produto)}) values(${stringify_object_values(novo_produto)})`

        banco.getConnection((error, conn) => {
            if(error) reject(error)

            conn.query(QUERY, (erro) => {
                conn.release()
                if(erro) reject(erro)

                resolve(novo_produto)
            })
        })
    })
}

module.exports = ProdutosSQL

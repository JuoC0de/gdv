const ProdutosSQL = require('../sql/produtos-sql')
const { arrange_object_types } = require('../utilities/object_functions')

class ProdutosController {
    static async buscar_produtos(req, res) {
        try {
            const produtos = await ProdutosSQL.buscar_produtos()
            return res.status(200).json(produtos)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async buscar_produto_por_codigo(req, res) {
        const { codigo} = req.params
        try {
            const produto = await ProdutosSQL.buscar_produto_por_codigo(codigo )
            return res.status(200).json(produto)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async buscar_produtos_por_codigo(req, res) {
        const { codigo, ativo } = req.params
        try {
            const produtos = await ProdutosSQL.buscar_produtos_por_codigo(codigo, ativo)
            return res.status(200).json(produtos)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async buscar_produtos_por_codigo_interno(req, res) {
        const { codigo_interno, ativo } = req.params
        try {
            const produtos = await ProdutosSQL.buscar_produtos_por_codigo_interno(codigo_interno, ativo)
            return res.status(200).json(produtos)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async buscar_produtos_por_descricao(req, res) {
        const { descricao, ativo } = req.params
        try {
            const produtos = await ProdutosSQL.buscar_produtos_por_descricao(descricao, ativo)
            return res.status(200).json(produtos)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async buscar_produto_por_descricao_e_marca(req, res) {
        const { descricao, marca, ativo } = req.params
        try {
            const produtos = await ProdutosSQL.buscar_produtos_por_descricao_e_marca(descricao, marca, ativo)
            return res.status(200).json(produtos)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async cadastrar_produto(req, res) {
        let novo_produto = req.body
        novo_produto = arrange_object_types(novo_produto)
        try {
            const novo_produto_cadastrado = await ProdutosSQL.cadastrar_produto(novo_produto)
            return res.status(201).json({
                "message": "Produto cadastrado com sucesso",
                "produto": novo_produto_cadastrado
            })
        } catch (error) {
            return res.status(501).json(error.message)
        }
    }
}

module.exports = ProdutosController

const express = require('express')
const server = express()
const port = process.env.PORT || 3000
const routes = require('./routes')

server.get('/', (req, res) => {
    return res.status(200).json({
        "mensagem": "api de conexão gdv em funcionamento"
    })
})

routes(server)

server.listen(port, () => {
    console.log(`Servidor em execução na porta ${port}`)
})

module.exports = server

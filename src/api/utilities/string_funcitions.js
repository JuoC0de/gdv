function stringify_object_values(novo_produto) {
    let novo_produto_arr_str = []
    for (const col in novo_produto) typeof novo_produto[col] == "string" ? novo_produto_arr_str.push(`'${novo_produto[col]}', `) : novo_produto_arr_str.push(`${novo_produto[col]}, `)
    novo_produto_arr_str = novo_produto_arr_str.join('')
    return novo_produto_arr_str.slice(0, novo_produto_arr_str.length-2)
}

function stringify_object_keys(novo_produto) {
    let columns = []
    for (const col in novo_produto) columns.push(`${col}, `)
    columns = columns.join('')
    return columns.slice(0, columns.length-2)
}

function stringify_object(novo_produto) {
    let novo_produto_arr_str = []
    for (const col in novo_produto) {
        novo_produto_arr_str.push(`${col}=`)
        typeof novo_produto[col] == "string" ? novo_produto_arr_str.push(`'${novo_produto[col]}', `) : novo_produto_arr_str.push(`${novo_produto[col]}, `)     
    }
    novo_produto_arr_str = novo_produto_arr_str.join('')
    return novo_produto_arr_str.slice(0, novo_produto_arr_str.length-2)
}

module.exports = {
    stringify_object,
    stringify_object_values,
    stringify_object_keys
}

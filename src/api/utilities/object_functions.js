function arrange_object_types(novo_produto) {
    let _novo_produto = novo_produto
    for (const col in _novo_produto) {
        if (col == 'data_ultimo_reajuste_preco') continue
        if (new RegExp('[0-9]', 'g').test(_novo_produto[col])) _novo_produto[col] = Number(_novo_produto[col])
    }
    return _novo_produto
}

module.exports = {
    arrange_object_types
}
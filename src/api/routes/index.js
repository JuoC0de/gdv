const cors = require('cors')
const body_parser = require('body-parser')
const produtos = require('./produtos-router.js')

module.exports = server => {
    server.use((req, res, next) => {
        res.header("Access-Control-Allow-Headers", "Content-Type")
        res.header("Access-Control-Allow-Origin", "*")
        res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE")
        server.use(cors())
        next()
    })
    server.use(body_parser.json())
    server.use(produtos)
}
const Router = require('express')
const routes = Router()
const ProdutosController = require('../controllers/produtos-controller')

routes.get('/produtos', ProdutosController.buscar_produtos)
routes.get('/produto/cod/:codigo', ProdutosController.buscar_produto_por_codigo)
routes.get('/produtos/cod/:codigo/ativo/:ativo', ProdutosController.buscar_produtos_por_codigo)
routes.get('/produtos/cod_interno/:codigo_interno/ativo/:ativo', ProdutosController.buscar_produtos_por_codigo_interno)
routes.get('/produtos/descricao/:descricao/ativo/:ativo', ProdutosController.buscar_produtos_por_descricao)
routes.get('/produtos/descricao/:descricao/marca/:marca/ativo/:ativo', ProdutosController.buscar_produto_por_descricao_e_marca)
routes.post('/produto', ProdutosController.cadastrar_produto)

module.exports = routes

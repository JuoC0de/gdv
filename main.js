const { app, BrowserWindow, globalShortcut, Menu } = require("electron");
const path = require("path");

let win;

function createWindow() {
  win = new BrowserWindow({
    width: 1600,
    height: 1000,
    /* alwaysOnTop: true, */
    //icon: "./icon.png",
    titleBarStyle: "hidden",
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, "/src/electron/preload.js"),
    },
  });

  Menu.setApplicationMenu(null);

  win.loadFile("./index.html");
}

app.whenReady().then(() => {
  createWindow();

  app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});
